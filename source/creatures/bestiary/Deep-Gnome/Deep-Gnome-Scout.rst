.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Глубинный гном разведчик (Deep Gnome Scout)
============================================================================================================

Глубинные гномы разведчики патрулируют туннели, ведущие в их поселения.
Некоторые ведут разведку в одиночку, чтобы максимально использовать свою скрытность, а другие ради безопасности объединяются в группы.



.. rst-class:: creature
.. _bestiary--Deep-Gnome-Scout:

Глубинный гном разведчик (`Deep Gnome Scout <https://2e.aonprd.com/Monsters.aspx?ID=93>`_) / Существо 1
------------------------------------------------------------------------------------------------------------

- :alignment:`Н`
- :size:`маленький`
- гном
- гуманоид

**Источник**: Bestiary pg. 74

**Восприятие**: +7;
:ref:`ночное зрение <cr_ability--Darkvision>`

**Языки**: Гномий,
Подземный

**Навыки**:
Акробатика +7,
Скрытность +7 (+10 под землей),
Выживание +5,
Природа +5

**Сил** +2,
**Лвк** +4,
**Тел** +2,
**Инт** +0,
**Мдр** +2,
**Хар** -1


**Предметы**:
легкий клевец,
праща (20 пуль)

----------

**КБ**: 17;
**Стойкость**: +7,
**Рефлекс**: +9,
**Воля**: +5

**ОЗ**: 18

----------

**Скорость**: 20 футов


**Ближний бой**: |д-1| легкий клевец +7 [+3/-1] (:w_agile:`быстрое`, :w_fatal:`фатальное d8`),
**Урон** 1d4+2 колющий


**Дальний бой**: |д-1| праща +9 [+4/-1] (шаг дистанции 50 фт, :w_propulsive:`тяговое`, перезарядка 1),
**Урон** 1d6+1 дробящий


**Врожденные природные заклинания** КС 14

| **1 ур.** :ref:`spell--i--Illusory-Disguise`


**Скрытое передвижение (Hidden Movement)**: Если глубинный гном разведчик начинает свой ход :c_undetected:`необнаруженным` или :c_hidden:`спрятанным` для существа, то это существо :c_flat_footed:`застигнуто врасплох` для атак данного разведчика до конца его хода.





.. include:: /helpers/actions.rst