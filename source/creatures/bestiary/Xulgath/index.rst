.. include:: /helpers/roles.rst

.. rst-class:: creature-details

***********************************************************************************************************
Зулгат (`Xulgath <https://2e.aonprd.com/MonsterFamilies.aspx?ID=102>`_)
***********************************************************************************************************

**Источник**: Bestiary pg. 336

Рептилоидные гуманоиды зулгаты (известные многим жителям поверхности как троглодиты), обитающие в самых верховьях Темноземья, часто нападают на нарушителей своих территорий лишь завидев их.
Они живут простыми семейными общинами, для выживания борясь с соперничающими группами и другими агрессивными обитателями Темноземья.
Время от времени они совершают набеги на поселения расположенные на поверхности, обычно по приказу жестоких, кровожадных вождей, которые сами часто находятся в подчинении у более могущественных существ, таких как :doc:`наги </creatures/bestiary/Naga/index>` или :doc:`демоны </creatures/bestiary/Demon/index>`.
Типичный зулгат обладает тускло-серой, темно-серой или пепельной чешуей, длинным хвостом и костяными выступами по всей длине позвоночника.
Рост типичного зулгата составляет 5 футов, а вес - 150 фунтов (~68 кг).

Хотя сегодня зулгаты жестоки и разрозненны, они были одними из первых разумных гуманоидов появившихся в первозданном мире, и когда-то правили могущественной империей, простиравшейся по всей территории Темноземья.
Сегодня от той эпохи остались лишь руины массивных каменных зиккуратов и разрушающиеся города, найденные в некоторых крупных пещерах.
Некоторые группы зулгатов продолжают жить среди этих руин, почитая достижения своих предков, а другие считают эти места запретными и оставляют их, чтобы те переполнились паразитами Темноземья.
Мудрецы не пришли к единому мнению о причинах падения древней цивилизации зулгатов.
Одни считают, что это произошло в результате поражения в нескольких войнах со :doc:`змеелюдами </creatures/bestiary/Serpentfolk/index>`, а другие полагают, что развращающее влияние поклонения демонам сгноило их культуру изнутри.

Конечно, многие поселения зулгатов продолжают поклоняться демонам и по сей день, воздавая почести и принося жертвоприношения ужасным существам из :ref:`Бездны <plane--Abyss>`.
Иногда зулгатский шаман может призвать и обуздать низшего демона, чтобы тот помогал группе, но шаман, слишком глубоко погрузившийся в оккультизм, может призвать более могущественного беса, который либо разорвет зулгатов на куски, либо поработит их.



.. toctree::
   :glob:
   :maxdepth: 3
   
   *



.. rst-class:: h3
.. rubric:: Сокровища зулгатов (Xulgath Treasure)

|treasure|

**Источник**: Bestiary pg. 336

Зулгаты относительно примитивны, используют инструменты и оружия из камня и других материалов, собранных с поверхности.
Они могут воспроизвести большинство простых и несколько воинских оружий ближнего боя, а также простое :w_thrown:`метательное` дистанционно оружие.
Развитое племя зулгатов может изготавливать низкоуровневые магические талисманы, такие как :ref:`item--Potency-crystal` или *возвращающая застежка* (чтобы это ни было).


.. rst-class:: h3
.. rubric:: Общество зулгатов (Xulgath Society)

|lore|

**Источник**: Bestiary pg. 337

Зулгатыы уважают силу, и лидер общины обычно является самым сильным из всех них.
Превосходство лидера редко ставится под сомнение, пока потенциально более сильный зулгат не решит, что пришло время сменить власть.
В этом случае два зулгата сталкиваются в смертельной схватке, и победитель получает власть.





.. include:: /helpers/actions.rst
.. include:: /helpers/bestiary-icons.rst