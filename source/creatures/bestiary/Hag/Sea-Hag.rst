.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Морская карга (Sea Hag)
============================================================================================================

Морские карги убивают и поедают рыбаков и моряков, которые приближаются к их логовам, мучают жителей прибрежных районов мрачными обещаниями и смутными угрозами, а также с удовольствием сеют смуту и раздор в маленьких городках.
Обычно они стараются не устраивать свои логова слишком близко к цивилизации, чтобы не навлекать врагов на свои дома.
Морские карги известны тем, что склоняют отчаявшихся жертв к трагическим и неизбежным сделкам, которые карга уже тайно склонила в свою пользу.
Несмотря на свой прожорливый аппетит, морские карги выглядят ужасно истощенными, и, в отличие от более могущественных карг у них нет умений для магической маскировки.

Морские карги могут вступать в ковены, но их склонность к воде, зачастую не позволяет им вступать в смешанные ковены с другими видами карг.



.. rst-class:: creature
.. _bestiary--Sea-Hag:

Морская карга (`Sea Hag <https://2e.aonprd.com/Monsters.aspx?ID=254>`_) / Существо 3
------------------------------------------------------------------------------------------------------------

- :alignment:`ХЗ`
- :size:`средний`
- земноводный
- карга
- гуманоид

**Источник**: Bestiary pg. 200

**Восприятие**: +10;
:ref:`ночное зрение <cr_ability--Darkvision>`

**Языки**: Акло,
Всеобщий,
Йотун

**Навыки**:
Атлетика +11,
Акробатика +8,
Обман +10,
Скрытность +8,
Оккультизм +8

**Сил** +4,
**Лвк** +3,
**Тел** +4,
**Инт** +1,
**Мдр** +3,
**Хар** +3


:ref:`cr_ability--Coven`: Морская карга добавляет к своим заклинаниям ковена :ref:`spell--a--Acid-Arrow`, :ref:`spell--m--Mariners-Curse` и :ref:`spell--w--Water-Walk`


**Сделка морской карги (Sea Hag's Bargain)**
(:t_occult:`оккультный`, :t_necromancy:`некромантия`)
Морская карга может заключить сделку с готовым существом, которое должно быть в здравом уме.
В обмен на какое-либо благо или обещание от морской карги, существо отдает особое или заветное качество - например, свою храбрость, красоту или голос.
Пока морская карга выполняет свою часть сделки, единственный способ вернуть утраченное качество - победить морскую ведьму или заключить другую сделку для его возвращения.

----------

**КБ**: 19;
**Стойкость**: +11,
**Рефлекс**: +8,
**Воля**: +10;
+1 состояния ко всем спасброскам от магии

**ОЗ**: 45

**Слабости**: :ref:`холодное железо <material--Cold-Iron>` 3

----------

**Скорость**: 25 футов,
плавание 35 футов


**Ближний бой**: |д-1| коготь +12 [+8/+4] (:w_agile:`быстрое`, :t_magical:`магический`),
**Урон** 1d10+4 рубящий


**Ужасающий взгляд (Dread Gaze)** |д-2|
(:t_occult:`оккультный`, :t_curse:`проклятие`, :t_fear:`страх`, :t_emotion:`эмоция`, :t_mental:`ментальный`)
Карга глядит на существо, вселяя в него мощный испуг и гложущее чувство надвигающегося рока, с результатами зависящими от спасброска Воли (КС 20).
Цели не нужно быть способной видеть морскую каргу.

| **Критический успех**: Нет эффекта.
| **Успех**: :c_frightened:`Напуган 1`.
| **Провал**: :c_frightened:`Напуган 1` и :c_slowed:`замедлен 1` на 1 раунд. Если цель была :c_dying:`при смерти`, то остается :c_unconscious:`без сознания` на 1 день. В конце дня она должна совершить спасбросок Стойкости с тем же КС; при провале, цель умирает.
| **Критический провал**: :c_frightened:`Напуган 2` и :c_slowed:`замедлен 1` на 1 минуту. Если цель была :c_dying:`при смерти`, то остается :c_unconscious:`без сознания` на 1 день. В конце дня она должна совершить спасбросок Стойкости с тем же КС; при провале, цель умирает.





.. include:: /helpers/actions.rst