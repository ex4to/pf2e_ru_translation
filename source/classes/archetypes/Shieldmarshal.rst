.. include:: /helpers/roles.rst

.. rst-class:: archetype
.. _archetype--Shieldmarshal:

Шилдмаршал (`Shieldmarshal <https://2e.aonprd.com/Archetypes.aspx?ID=197>`_)
------------------------------------------------------------------------------------------------------------

**Источник**: Lost Omens: Impossible Lands pg. 108

Для соблюдения законов Алкенстара недостаточно только стрельбы; когда огнестрельное оружие легко доступно преступникам и коррупционерам, полагаться только на огневую мощь не всегда лучший подход для охраны правопорядка.
Чтобы оставаться впереди в бесконечной войне с преступностью и шпионажем, элитные шилдмаршалы Алкенстара совершенствуют свои навыки меткой стрельбы и ситуационной осведомленности в мощную смесь оперативной проницательности и специальных тактик.
Шилдмаршалы изучают криминалистику, инженерное дело, право и этикет, чтобы ориентироваться в гремучих условиях и политике этого города; продвинутые уроки включают специальную подготовку по борьбе с преступниками с помощью технологий, а также тактический анализ для маневрирования в перегруженных городских условиях.

Вы прошли специальную подготовку, которая поможет вам поддерживать мир на разросшихся улицах Алкенстара.
Городские операции ужасно сложны; закрытые двери и высокие стены скрывают подозреваемых и враждующих неприятелей, а каждый переулок и коридор может стать смертельным тупиком.
Как шилдмаршал, вы преодолеваете эти сложности, сокращая неопределенности и адаптируясь к ним.
Вы методично наблюдаете за обстановкой, быстро принимаете решения и используете передовой опыт для усмирения врагов, спасения заложников и достижения общих целей миссии.


.. _arch-feat--Shieldmarshal--Dedication:

Посвящение шилдмаршала (`Shieldmarshal Dedication <https://2e.aonprd.com/Feats.aspx?ID=4040>`_) / 2
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- :uncommon:`необычный`
- посвящение
- архетип

**Доступ**: Персонаж из Алкенстара

**Источник**: Lost Omens: Impossible Lands pg. 109

----------

Благодаря своей элитной шилдмаршальской подготовке вы одинаково хорошо чувствуете себя и в убожестве Смоуксайда, и в великолепии Скайсайда.
Вы хорошо знакомы с лабиринтами городских улиц и уставами.
Вы становитесь обучены Обществу; если вы уже были ему обучены, то вместо этого становитесь экспертом Общества.
В городской среде вы можете совершать проверки Общества чтобы :ref:`skill--Survival--Sense-Direction`.
Еще вы получаете способности навыка :ref:`feat--Courtly-Graces` и :ref:`feat--Streetwise`.

**Особенность**: Вы не можете выбрать другую способность посвящения, пока не получите 2 другие способности из архетипа шалдмаршала.


.. _arch-feat--Shieldmarshal--Consolidated-Overlay-Panopticon:

Объединенный оверлей Паноптикум (`Consolidated Overlay Panopticon <https://2e.aonprd.com/Feats.aspx?ID=4041>`_) |д-1| / 4
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- архетип

**Предварительные условия**: :ref:`arch-feat--Shieldmarshal--Dedication`

**Частота**: раз в час

**Источник**: Lost Omens: Impossible Lands pg. 109

----------

Вы добровольно согласились на неоднозначную экспериментальную процедуру по установке заводных протезов глаз.
Эти глаза передают информацию о том, что вы видите, в штаб шилдмаршалов, который собирает и анализирует данные об окружении - вероятность наступления Бронзового Времени\* или Времени Всплеска\*, давление, температуру, ветер и так далее - и возвращает их вам.
Железный Магистр периодически просматривает архивы информации в рамках своей постоянной работы по искоренению полицейской коррупции.
Когда вы используете свой Объединенный оверлей Паноптикум, вы усиливаете глазные протезы; на следующую минуту вы получаете ночное зрение и сумеречное зрение, а также бонус состояния +1 к визуальным проверкам Восприятия.
Даже когда протезы глаз не усилены, вы сохраняете нормальное зрение.

\* - Примечание переводчика: В Алкенстаре есть часы Бронзового Времени, когда магический фон более стабилен и погода предсказуема, а во Время Всплеска возрастает вероятность магических выбросов и перемен погоды.


.. _arch-feat--Shieldmarshal--Equitable-Defense:

Эквивалентная защита (`Equitable Defense <https://2e.aonprd.com/Feats.aspx?ID=4042>`_) |д-р| / 8
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- архетип

**Предварительные условия**: :ref:`arch-feat--Shieldmarshal--Dedication`

**Частота**: раз в 10 минут

**Триггер**: Вы получаете урон от критического попадания

**Источник**: Lost Omens: Impossible Lands pg. 109

----------

Стиснув зубы от боли, вы готовитесь к контратаке.
Вы получаете сопротивление урону от этого критического попадания, равное половине вашего уровня.
Пока вы остаетесь в сознании после данной атаки, вы можете :ref:`action--Interact`, чтобы перезарядить оружие которым владеете в данный момент, или :ref:`action--Stand`.
Вы получаете бонус обстоятельства +1 к следующему :ref:`Удару (Strike) <action--Strike>` по существу, которое нанесло вам критическое попадание, при условии, что вы нанесете его до конца своего следующего хода.


.. _arch-feat--Shieldmarshal--Counterclockwork-Focus:

Антизаводное сосредоточение (`Counterclockwork Focus <https://2e.aonprd.com/Feats.aspx?ID=4043>`_) |д-1| / 10
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- архетип

**Предварительные условия**: :ref:`arch-feat--Shieldmarshal--Dedication`

**Частота**: раз в раунд

**Источник**: Lost Omens: Impossible Lands pg. 109

----------

Вы понимаете необходимость адаптации правоохранительной доктрины к постоянно меняющимся инновациям в области технологий и арканы.
Ваша подготовка в области специального оружия и тактики дает вам доступ к необычным методам реагирования на преступления, связанные с заводными механизмами и магией.
Вы применяете специальные добавки к заряженному огнестрельному оружию.
При вашем следующем :ref:`Ударе (Strike) <action--Strike>` этим огнестрельным оружием до конца вашего хода вы наносите дополнительные 2d6 урона, если цель - :t_construct:`конструкт`, и по своему выбору можете считать амуницию :ref:`холодным железом <material--Cold-Iron>` или :ref:`адамантином <material--Adamantine>`.
На 18-м уровне этот дополнительный урон увеличивается до 3d6, и вы можете по своему выбору считать амуницию :ref:`орихалком <material--Orichalcum>`.





.. include:: /helpers/actions.rst