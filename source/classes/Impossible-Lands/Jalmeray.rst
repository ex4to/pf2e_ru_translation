.. include:: /helpers/roles.rst

.. _LOIL--Adventuring-in-Jalmeray:

============================================================================================================
Приключения в Джалмерэе (Adventuring in Jalmeray)
============================================================================================================

.. epigraph::

	*Рынки Джалмерэя, являющегося вратами в Вудру во Внутреннем Море - это рассадник иностранных торговцев, ищущих необычные товары.
	Искателей приключений из Джалмерэя часто останавливают в своих путешествиях коллекционеры, желающие осмотреть необычное существо или предмет снаряжения.*

-----------------------------------------------------------------------------


.. _Jalmeray--Familiar:

Особый фамильяр церу (Ceru Specific Familiar)
--------------------------------------------------------------------------------------------------------

**Источник**: Lost Omens: Impossible Lands pg. 220

Добавлен новый особый фамильяр фамильяр :ref:`specific-familiar--Ceru`.



.. _Jalmeray--Weapons:

Новое оружие (New Weapons)
--------------------------------------------------------------------------------------------------------

**Источник**: Lost Omens: Impossible Lands pg. 220

Хотя это оружие :r_uncommon:`необычно` где-то в других местах, персонажи из Джалмерэя или Вудры имеют доступ к чакри, дончаку, калису, панабасу и зульфикару.
Висап имеет признак :w_injection:`инъекционное`.

Воинское ближнего боя
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:ref:`table--6-7-2`, :ref:`table--6-7-3`

* :ref:`weapon--Kris`
* :ref:`weapon--Panabas`
* :ref:`weapon--Talwar`
* :ref:`weapon--Dandpatta`
* :ref:`weapon--Thorn-Whip`

Продвинутое ближнего боя
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:ref:`table--6-7-4-1`, :ref:`table--6-7-4`

* :ref:`weapon--Broadspear`
* :ref:`weapon--Gada`
* :ref:`weapon--Donchak`
* :ref:`weapon--Kalis`
* :ref:`weapon--Visap`
* :ref:`weapon--Zulfikar`

Продвинутое дистанционное
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:ref:`table--6-8-3`

* :ref:`weapon--Chakri`




.. _Jalmeray--Student-of-Perfection:

Ученик Совершенства (Student of Perfection)
--------------------------------------------------------------------------------------------------------

**Источник**: Lost Omens: Impossible Lands pg. 222

Четыре Дома Совершенства в Джалмерэе постоянно открывают новые приемы и умения, чтобы обновить и усовершенствовать свои стили и одержать верх в следующем Испытании Небесной Тверди.
Способности, представленные в архетипе :doc:`Ученика Совершенства </classes/archetypes/dlc/student-of-perfection>` из книги *Lost Omens: World Guide* - лишь малая часть этих обширных стилей.
Следующие способности архетипа представляют для Ученика Совершенства больше опций, но есть еще бесчисленное множество скрытых техник, которыми владеют лишь отдельные монахи или которые передаются в тайне от мастера к ученику.

**Способности**

* :ref:`arch-feat--Student-of-Perfection--Perfect-Weaponry`
* :ref:`arch-feat--Student-of-Perfection--Perfect-Resistance`
* :ref:`arch-feat--Student-of-Perfection--Perfect--Ki-Expert`
* :ref:`arch-feat--Student-of-Perfection--Perfect--Ki-Exemplar`
* :ref:`arch-feat--Student-of-Perfection--Perfect--Ki-Grandmaster`

**Заклинания ки**

* :ref:`spell--focus--Unblinking-Flame-Aura`
* :ref:`spell--focus--Unbreaking-Wave-Vapor`
* :ref:`spell--focus--Unfolding-Wind-Buffet`
* :ref:`spell--focus--Untwisting-Iron-Roots`
* :ref:`spell--focus--Unblinking-Flame-Emblem`
* :ref:`spell--focus--Unbreaking-Wave-Barrier`
* :ref:`spell--focus--Unfolding-Wind-Blitz`
* :ref:`spell--focus--Untwisting-Iron-Augmentation`
* :ref:`spell--focus--Unblinking-Flame-Ignition`
* :ref:`spell--focus--Unbreaking-Wave-Containment`
* :ref:`spell--focus--Unfolding-Wind-Crash`
* :ref:`spell--focus--Untwisting-Iron-Pillar`



.. _Jalmeray--Jalmeri-Heavenseeker:

Джалмерэйский Искатель Небес (Jalmeri Heavenseeker)
--------------------------------------------------------------------------------------------------------

**Источник**: Lost Omens: Impossible Lands pg. 224

Добавлен архетип :doc:`/classes/archetypes/Jalmeri-Heavenseeker` и его :doc:`заклинания фокусировки </spells/focus/Jalmeri-Heavenseeker>`.





.. include:: /helpers/actions.rst